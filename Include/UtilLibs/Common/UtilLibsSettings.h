//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                      ---  Utility  Library  ---                      **
**                                                                      **
**          Copyright (C), 2001 - 2016, Takahiro Itou                   **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      プロジェクトの設定。
**
**      @file       Common/UtilLibsSettings.h
**/

#if !defined( UTILLIBS_COMMON_INCLUDED_UTIL_LIBS_SETTINGS_H )
#    define   UTILLIBS_COMMON_INCLUDED_UTIL_LIBS_SETTINGS_H

//  スクリプトによる設定値が書き込まれたヘッダを読み込む。  //
#include    "UtilLibs/.Config/ConfiguredUtilLibs.h"

UTILLIBS_NAMESPACE_BEGIN

UTILLIBS_NAMESPACE_END

#endif
